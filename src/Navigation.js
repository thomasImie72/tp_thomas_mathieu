import React from 'react';
import Content from './Content.js'
import Button from './Button.js'

import NavBar from './simpleAppBar';


const Navigation = ({loginFunction, logoutFunction,isLogged}) => {
    return (
        <div>
            <header>
                <NavBar/>
                <ul>
                    <li><a href="/">Home</a></li>
                    <li><a href="/utilisateurs">Utilisateur</a></li>
                    <li><a href="/articles">Articles</a></li>
                    <li><a href="/albums">Albums</a></li>
                    {isLogged ? (
                        <Button
                        nameFunction={logoutFunction}
                        children = "Logout"
                        nomClasse = "logoutButton"
                        itemId = {null}
                        />
                    ):(
                        <Button
                        nameFunction={loginFunction}
                        children = "Login"
                        nomClasse = "loginButton"
                        itemId = {null}
                        />
                    )}
                    <p>Etat : {isLogged ? "Connecté":"Déconnecté"}</p>
                </ul>
            </header>
            <main>
                <Content
                    isLogged = {isLogged}
                />
            </main>
        </div>
    )
}

export default Navigation;
