const DEFAULT_STATE = {
    listUtilisateurs: [],
    listAlbumUtilisateurs: [],
    listTodosUtilisateurs:[],
    listPostsUtilisateurs: [],
    listPhotoAlbum: [],
    isLoading: false,
    isLogged: true
  }

export default DEFAULT_STATE;
