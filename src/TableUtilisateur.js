import React from 'react';
import Button from './Button.js';
import PropTypes from 'prop-types';

const TableUtilisateur = ({listUtilisateur, onDismiss,showAlbumUser, showTodosUser, showPostsUser}) => {
    return(
        <div className="Table" align="center">
            <table>
                <thead>
                    <tr>
                        <th colspan="1">Username</th>
                        <th colspan="1">Name</th>
                        <th colspan="1">Email</th>
                        <th colspan="1">Action</th>
                    </tr>
                </thead>
                <tbody>
                    {listUtilisateur.map(item =>
                        <tr>
                            <td>{item.username}</td>
                            <td>{item.name}</td>
                            <td>{item.email}</td>
                            <td>
                                <Button
                                    nameFunction={showAlbumUser}
                                    children = "Albums"
                                    nomClasse = "showAlbumUserButton"
                                    itemId = {item.id}
                                />
                                <Button
                                    nameFunction={showPostsUser}
                                    children = "Posts"
                                    nomClasse = "showPostsUserButton"
                                    itemId = {item.id}
                                />
                                <Button
                                    nameFunction={showTodosUser}
                                    children = "Todos"
                                    nomClasse = "showTodosUserButton"
                                    itemId = {item.id}
                                />
                                <Button
                                    nameFunction={onDismiss}
                                    children = "Supprimer"
                                    nomClasse = "dimsissButton"
                                    itemId = {item.id}
                                />
                            </td>
                        </tr>
                    )}
                </tbody>
            </table>
        </div>
    )
}

export default TableUtilisateur;
