import React, { Component } from 'react';
import Navigation from './Navigation';
import './App.css';
import DEFAULT_STATE from './initState'

class App extends Component {

  constructor(props){
    super(props);
    this.state = {
        ...DEFAULT_STATE
    };
  }

  login = () => {
    this.setState({isLogged: true})
  }

  logout = () => {
    this.setState({isLogged: false})
  }

  render() {
    return (
      <div className="App">
        <Navigation
          loginFunction={this.login}
          logoutFunction = {this.logout}
          isLogged= {this.state.isLogged}
        />
      </div>
    );
  }
}

export default App;

