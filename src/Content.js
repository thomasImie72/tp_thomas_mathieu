import React from 'react';
import Home from './Home.js';
import NotFound from './NotFound.js';
import { ArticlesWithConnected,AlbumsWithConnected } from './NotConnected';
import { UtilisateursWithConnected } from './NotConnected'
import { Route, Switch } from 'react-router';

const Content = (props) => {
    return (
        <Switch>
            <Route exact path='/' component={Home} />
            <Route exact path='/articles'  render={() => <ArticlesWithConnected {...props} />} />
            <Route exact path='/albums' render={() => <AlbumsWithConnected {...props} />} />
            <Route exact path='/utilisateurs' render={() => <UtilisateursWithConnected {...props} />}/>
            <Route path="*" component={NotFound} />
        </Switch>
    )
}

export default Content;
