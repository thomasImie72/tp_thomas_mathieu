import Button from './Button.js'
import { Utilisateurs } from './components/Utilisateurs'
import Articles from './components/Articles'
import Albums from './components/Albums'
import React from 'react';

const NotConnected = () => {
    return (
    <div>
        <h2>Veuillez-vous connecter ! </h2>
    </div>
    )}

const withConnected = (Component) => ({ isLogged, ...props }) => {
    return !isLogged
        ? <NotConnected />
        : <Component { ...props } />
}

export const UtilisateursWithConnected = withConnected(Utilisateurs);
export const ArticlesWithConnected = withConnected(Articles);
export const AlbumsWithConnected = withConnected(Albums);