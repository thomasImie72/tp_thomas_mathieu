import React from 'react';

const NotFound =({ message }) => {
    return (
        <div className="NotFound">
            <h2>Erreur 404 - Page not found</h2>
        </div>
    )
}

export default NotFound;
