import React from 'react';

export const Form = ({ className, handleSubmit, handleChange, state, validateForm }) =>

    <div className={className}>
        <form onSubmit={handleSubmit}>
            <input onChange={handleChange} value={state.title} id="title" type="text" placeholder="titre" required /><br></br>
            <input onChange={handleChange} value={state.body} id="body" type="text" placeholder="contenu" required /><br></br>
            <button type="submit" disabled={!validateForm()}>Ajouter</button>
        </form>
    </div>
