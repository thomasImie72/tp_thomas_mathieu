import React from 'react';

export const Todos = ({ display, className, listTodos }) =>
    <div className={className}>
        {display ? (
            <table align="center">
                <caption><h2><b>Todos de l'utilisateur</b></h2></caption>
            <thead>
                <tr>
                    <th colspan="1">Title</th>
                    <th colspan="1">Completed</th>
                </tr>
            </thead>
            <tbody>
                {listTodos.map(item =>
                    <tr>
                        <td>{item.title}</td>
                        <td>{item.completed ? 'Yes' : 'No'}</td>
                    </tr>
                )}
            </tbody>
        </table>
        ) : (
            null
        )}
    </div>
