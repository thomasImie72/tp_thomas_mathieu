import React, { Component } from 'react';
import DEFAULT_STATE from '../initState'
import axios from 'axios';
import TableUtilisateur from '../TableUtilisateur';
import { Todos } from './Todos'
import { AlbumsUtilisateur } from './AlbumsUtilisateur';
import { Posts } from './Posts';

const DEFAULT_QUERY = 'redux';
const PATH_BASE = 'https://jsonplaceholder.typicode.com/users';
const PATH_ALBUM_USER = 'https://jsonplaceholder.typicode.com/albums?userId=';
const PATH_TODO_USER = 'https://jsonplaceholder.typicode.com/todos?userId=';
const PATH_POSTS_USER = 'https://jsonplaceholder.typicode.com/posts?userId='

export class Utilisateurs extends Component {

    constructor(props) {
        super(props);
        this.state = {
            ...DEFAULT_STATE
        };
    }

    onDismiss = id => {
        const updatedList = this.state.listUtilisateurs.filter(item => item.id !== id);
        this.setState({listUtilisateurs:updatedList});
      }

    showAlbumUser = id => {
        this.setState({showListAlbum:true});
        this.getAlbumUtilisateur(id);
    }

    showTodosUser = id => {
        this.setState({showListTodos:true});
        this.getTodosUtilisateur(id);
    }

    showPostsUser = id => {
        this.setState({showListPosts:true});
        this.getPostsUtilisateur(id);
    }

    getData = () => {
        const { searchTerm } = this.state;

        axios.get(`${PATH_BASE}`)
            .then(
                result => {
                    this.setState({
                        listUtilisateurs: result.data
                    })
                }
            )
            .catch(error => error);
    }

    getAlbumUtilisateur = (id) => {
        axios.get(`${PATH_ALBUM_USER}${id}`)
            .then(
                result => {
                    this.setState({
                        listAlbumUtilisateurs: result.data
                    })
                }
            )
            .catch(error => error);
    }

    getTodosUtilisateur = (id) => {
        axios.get(`${PATH_TODO_USER}${id}`)
            .then(
                result => {
                    this.setState({
                        listTodosUtilisateurs: result.data
                    })
                
                }
            )
            .catch(error => error);
    }

    getPostsUtilisateur = (id) => {
        axios.get(`${PATH_POSTS_USER}${id}`)
            .then(
                result => {
                    this.setState({
                        listPostsUtilisateurs: result.data
                    })
                }
            )
            .catch(error => error);
    }

    componentDidMount = () => {
        this.getData();
    }

    render() {
        const { listUtilisateurs } = this.state;
        if (!listUtilisateurs) {
            return null;
        }

        return (
            <div className="listUtilisateurs">
                <h2>Utilisateurs</h2>
                <TableUtilisateur
                    listUtilisateur = {listUtilisateurs}
                    onDismiss = {this.onDismiss}
                    showAlbumUser = {this.showAlbumUser}
                    showTodosUser = {this.showTodosUser} 
                    showPostsUser = {this.showPostsUser}
                />
                <Todos
                    display = {this.state.showListTodos}
                    className = "todosListUser"
                    listTodos = {this.state.listTodosUtilisateurs}
                />
                <AlbumsUtilisateur
                    display = {this.state.showListAlbum}
                    className = "albumListUser"
                    listAlbums = {this.state.listAlbumUtilisateurs}
                />
                <Posts
                    display = {this.state.showListPosts}
                    className = "albumListUser"
                    listPosts = {this.state.listPostsUtilisateurs}
                />
            </div>
        )
    }

}