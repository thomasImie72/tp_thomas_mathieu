import React from 'react';

export const Details = ({ display, className, body }) =>
    <div className={className}>
        {display ? (
            <p>{body}</p>
        ) : (
            null
        )}
    </div>
