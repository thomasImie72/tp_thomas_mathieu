import React from 'react';

export const AlbumsUtilisateur = ({ display, className, listAlbums }) =>
    <div className={className}>
        {display ? (
            <table align="center">
                <caption><h2><b>Albums de l'utilisateur</b></h2></caption>
            <thead>
                <tr>
                    <th colspan="1">Title</th>
                </tr>
            </thead>
            <tbody>
                {listAlbums.map(item =>
                    <tr>
                        <td>{item.title}</td>
                    </tr>
                )}
            </tbody>
        </table>
        ) : (
            null
        )}
    </div>
