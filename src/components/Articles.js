import React, { Component } from 'react';
import axios from 'axios';
import { Search } from './Search';
import { Form } from './Form';
import { Details } from './Details';
import Button from '../Button.js'

const PATH_BASE = 'https://jsonplaceholder.typicode.com/posts';

export class Articles extends Component {

    displayDetails = false;

    constructor(props) {
        super(props);

        this.state = {
            data: [],
            displayDetails: false,
            title: '',
            body: '',
            message: ''
        };
    }

    getData = () => {
        axios.get(`${PATH_BASE}`)
            .then(
                result => {
                    this.setState({
                        data: result.data
                    })
                }
            )
            .catch(error => error);
    }

    componentDidMount() {
        this.getData();
    }

    onSearchChange = event => {
        let res = this.state.data.slice(0, 10).filter(items => {
            return items.title.toLowerCase().includes(event.target.value);
        }
        );
        this.setState({
            data: res
        })
    }

    showDetails() {
        // console.log(this.state);
        // if(this.state.displayDetails == true) {
        //     this.setState({
        //         displayDetails: false
        //     })
        // } else {
        //     this.setState({
        //         displayDetails: true
        //     })
        // }
    }

    onDismiss = id => {
        const updatedList = this.state.data.filter(item => item.id !== id);
        this.setState({ data: updatedList });
    }

    validateForm = () => {
        return this.state.title.length < 10
            && this.state.body.length < 20;
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    };

    handleSubmit = e => {
        e.preventDefault();
        const article = { title: this.state.title, body: this.state.body };
        this.state.data.unshift(article);
        this.setState({
            data: this.state.data,
            message: 'Article "' + this.state.title + '" ajouté'
        });
        this.state.title = '';
        this.state.body = '';
    };

    render() {
        const { data } = this.state;

        if (!data) {
            return null;
        }

        return (
            <div className="Articles">
                <Form className="Login"
                    handleSubmit={this.handleSubmit}
                    handleChange={this.handleChange}
                    state={this.state}
                    validateForm={this.validateForm}>
                </Form>
                <p><i>{this.state.message}</i></p>
                <hr></hr>
                <Search
                    className="Input"
                    onChange={this.onSearchChange}
                >
                    Search
                </Search>
                <hr></hr>
                {data.slice(0, 10).map(item =>
                    <ul>
                        <li>
                            <h2 onClick={this.showDetails}>{item.title}</h2>
                            <p>{item.body}</p>
                        </li>
                        <Button
                            nameFunction={this.onDismiss}
                            children="Supprimer"
                            nomClasse="dimsissButton"
                            itemId={item.id}
                        />
                        <Details
                            display={this.state.displayDetails}
                            className="details"
                            body={item.body}></Details>
                    </ul>
                )}
            </div>
        )
    }

}

export default Articles;