import React, { Component } from 'react';
import axios from 'axios';
import { Photo } from './Photo';

const DEFAULT_QUERY = 'redux';
const PATH_BASE = 'https://jsonplaceholder.typicode.com/albums';
const PATH_IMAGE_ALBUM = 'https://jsonplaceholder.typicode.com/photos?albumId=';

export class Albums extends Component {

    constructor(props) {
        super(props);

        this.state = {
            data: []
        };
    }

    getData = () => {

        axios.get(`${PATH_BASE}`)
            .then(
                result => {
                    this.setState({
                        data: result
                    })
                }
            )
            .catch(error => error);
    }

    getPhotoAlbum = (id) => {
        axios.get(`${PATH_IMAGE_ALBUM}${id}`)
            .then(
                result => {
                    this.setState({
                        listPhotoAlbum: result.data
                    })
                }
            )
            .catch(error => error);
    }

    showPhotoAlbum = id => {
        this.setState({showPhotoAlbum:true});
        this.getPhotoAlbum(id);
    }


    componentDidMount() {
        this.getData();
    }

    handleClick = (item, e) => {
        this.showPhotoAlbum(item.id);
    }

    render() {
        const { data } = this.state.data;

        if (!data) {
            return null;
        }

        return (
            <div className="Albums">
                {data.map(item =>
                <ul>
                    <li>
                        <h2 /*onClick={this.handleClick.bind(this, item)}*/>{item.title}</h2>
                    </li>
                </ul>
                )}
                <Photo
                    display = {this.state.showPhotoAlbum}
                    className = "photoAlbumList"
                    listPhotoAlbum = {this.state.listPhotoAlbum}
                />
            </div>
        )
    }

}

export default Albums;