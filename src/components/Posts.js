import React from 'react';

export const Posts = ({ display, className, listPosts }) =>
    <div className={className}>
        {display ? (
            <table align="center">
                <caption><h2><b>Posts de l'utilisateur</b></h2></caption>
            <thead>
                <tr>
                    <th colspan="1">Title</th>
                    <th colspan="1">Body</th>
                </tr>
            </thead>
            <tbody>
                {listPosts.map(item =>
                    <tr>
                        <td>{item.title}</td>
                        <td>{item.body}</td>
                    </tr>
                )}
            </tbody>
        </table>
        ) : (
            null
        )}
    </div>
