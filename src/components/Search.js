import React from 'react';

export const Search = ({ className, onChange, componentLoaded, children }) => 
{
    return (
        <form>
            {children}<input
                className={className}
                placeholder="rechercher" type="text"
                onChange={onChange}/>
        </form>
    )
}
