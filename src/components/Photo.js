import React from 'react';

export const Photo = ({ display, className, listPhotoAlbum }) =>
    <div className={className}>
        {display ? (
            <table align="center">
                <caption><h2><b>Photo de l'album</b></h2></caption>
            <thead>
                <tr>
                    <th colspan="1">Title</th>
                    <th colspan="1">Photo</th>
                </tr>
            </thead>
            <tbody>
                {listPhotoAlbum.map(item =>
                    <tr>
                        <td>{item.title}</td>
                        <td><img src={item.url} /></td>
                    </tr>
                )}
            </tbody>
        </table>
        ) : (
            null
        )}
    </div>
