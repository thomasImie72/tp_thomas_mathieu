import React from 'react';

const Button =({ nameFunction, children, nomClasse, itemId }) => {
    return (
        <div className="Button">
            <button className={nomClasse} onClick={() => nameFunction(itemId)}>{children}</button>
        </div>
    )
}

export default Button;
